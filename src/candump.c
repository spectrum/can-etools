/*
 * This file is part of the can-etools package
 *
 * Copyright (c) 2002-2007 Volkswagen Group Electronic Research
 * Copyright (c) 2021 Angelo Dureghello <angelo@kernel-space.org>.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <net/if.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/uio.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "version.h"

#define MAXSOCK 16
#define hex_asc_upper_lo(x) hex_asc_upper[((x) & 0x0F)]
#define hex_asc_upper_hi(x)	hex_asc_upper[((x) & 0xF0) >> 4]

static volatile int running = 1;
const char hex_asc_upper[] = "0123456789ABCDEF";

static void usage(void)
{
	printf("can-etools v." VERSION " canbus embedded tools.\n");
	printf("(C) 2021 A. Dureghello\n");
	printf("Usage: candump INTERFACE\n");
	printf("Example: candump can0\n");

	_exit(1);
}

static void insert_id(char *buff, int id, int eff)
{
	int eoff;

	eoff = eff ? 7 : 2;

	/* eff = 29bit, sff = 11 */
	while (eoff >= 0) {
		buff[eoff--] = hex_asc_upper_lo(id);
		id >>= 4;
	}
}

static void put_hex_byte(char *buf, char byte)
{
	buf[0] = hex_asc_upper_hi(byte);
	buf[1] = hex_asc_upper_lo(byte);
}

static void display_frame(struct canfd_frame *cf)
{
	char buff[16] = {0};
	int offset, i;

	/*
	 * Ext address takes 8 bytes
	 */
	if (cf->can_id & CAN_ERR_FLAG) {
		buff[8] = '#';
		insert_id(buff, cf->can_id & (CAN_ERR_MASK|CAN_ERR_FLAG), 1);
		offset = 9;
	} else if (cf->can_id & CAN_EFF_FLAG) {
		buff[8] = '#';
		insert_id(buff, cf->can_id & CAN_EFF_MASK , 1);
		offset = 9;
	} else {
		/* simple frame */
		buff[3] = '#';
		insert_id(buff, cf->can_id , 0);
		offset = 4;
	}

	printf("\x1b[1;32m");
	for (i = 0; i < offset - 1; i++)
		printf("%c", buff[i]);

	printf("\x1b[1;36m");
	printf("%c", buff[offset - 1]);

	printf("\x1b[1;33m");
	for (i = 0; i < cf->len; i++) {
		put_hex_byte(buff + offset, cf->data[i]);
		printf("%c%c", *(buff + offset), *(buff + offset + 1));
		offset += 2;
	}
	printf("\x1b[0m\n");
}

static int dump(char *iface)
{
	int fd_epoll, s, nbytes, num_events;
	int i, currmax;
	int rcvbuf_size = 0;
	int timeout_ms = -1;
	struct sockaddr_can addr;
	struct epoll_event events_pending[MAXSOCK];
	struct epoll_event event_setup = {
		.events = EPOLLIN, /* prepare the common part */
	};
	struct ifreq ifr;
	struct iovec iov;
	struct msghdr msg;
	struct canfd_frame frame;
	char ctrlmsg[CMSG_SPACE(sizeof(struct timeval) +
		3 * sizeof(struct timespec) + sizeof(__u32))];

	running = 1;

	fd_epoll = epoll_create(1);
	if (fd_epoll < 0) {
		perror("epoll_create");
		return 1;
	}

	s = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (s < 0) {
		perror("socket");
		return 1;
	}

	if (epoll_ctl(fd_epoll, EPOLL_CTL_ADD, s, &event_setup)) {
		perror("failed to add socket to epoll");
		return 1;
	}

	addr.can_family = AF_CAN;

	memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
	strncpy(ifr.ifr_name, iface, strlen(iface));

	if (ioctl(s, SIOCGIFINDEX, &ifr) < 0) {
		perror("SIOCGIFINDEX");
		exit(1);
	}
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}

	iov.iov_base = &frame;
	msg.msg_name = &addr;
	msg.msg_iov = &iov;
	msg.msg_iovlen = 1;
	msg.msg_control = &ctrlmsg;

	printf("candump listening ...\n");

	while (running) {
		num_events = epoll_wait(fd_epoll, events_pending,
						1, timeout_ms);
		if (num_events == -1) {
			if (errno != EINTR)
				running = 0;
			continue;
		}
		for (i = 0; i < num_events; i++) {
			struct if_info* obj = events_pending[i].data.ptr;
			int idx;
			char *extra_info = "";

			/* these settings may be modified by recvmsg() */
			iov.iov_len = sizeof(frame);
			msg.msg_namelen = sizeof(addr);
			msg.msg_controllen = sizeof(ctrlmsg);
			msg.msg_flags = 0;

			nbytes = recvmsg(s, &msg, 0);

			if (nbytes < 0) {
				if (errno == ENETDOWN) {
					fprintf(stderr, "interface down\n");
					continue;
				}
				perror("read");
				return 1;
			}

			if (nbytes == 16) {
				display_frame(&frame);
			}

		}
	}

	return 0;
}

int main(int argc, char **argv)
{
	int c;

	if (argc < 2)
		usage();

        while (1) {
                int this_option_optind = optind ? optind : 1;
                int option_index = 0;
                static struct option long_options[] = {
                        {"help", no_argument, 0, 'h'},
                        {0, 0, 0, 0}
                };

                c = getopt_long(argc, argv, "h",
                                long_options, &option_index);
                if (c == -1)
                        break;

                switch (c) {
                case 'h':
                        usage();
                        break;
                default:
                        _exit(2);
                }
        }

	return dump(argv[1]);
}
