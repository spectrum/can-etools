/*
 * This file is part of the can-etools package
 *
 * Copyright (c) 2002-2007 Volkswagen Group Electronic Research
 * Copyright (c) 2021 Angelo Dureghello <angelo@kernel-space.org>.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 2.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <net/if.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "version.h"

#define CANID_DELIM '#'
#define CC_DLC_DELIM '_'
#define DATA_SEPERATOR '.'

static void usage(void)
{
	printf("can-etools v." VERSION " canbus embedded tools.\n");
	printf("(C) 2021 A. Dureghello\n");
	printf("Usage: cansend INTERFACE FRAME\n");
	printf("Example: cansend can0 01a#11223344AABBCCDD\n");

	_exit(1);
}

static const unsigned char dlc2len[] = {0, 1, 2, 3, 4, 5, 6, 7,
					8, 12, 16, 20, 24, 32, 48, 64};

static unsigned char can_fd_dlc2len(unsigned char dlc)
{
	return dlc2len[dlc & 0x0F];
}

static const unsigned char len2dlc[] =
	{0, 1, 2, 3, 4, 5, 6, 7, 8,		/* 0 - 8 */
	 9, 9, 9, 9,				/* 9 - 12 */
	 10, 10, 10, 10,			/* 13 - 16 */
	 11, 11, 11, 11,			/* 17 - 20 */
	 12, 12, 12, 12,			/* 21 - 24 */
	 13, 13, 13, 13, 13, 13, 13, 13,	/* 25 - 32 */
	 14, 14, 14, 14, 14, 14, 14, 14,	/* 33 - 40 */
	 14, 14, 14, 14, 14, 14, 14, 14,	/* 41 - 48 */
	 15, 15, 15, 15, 15, 15, 15, 15,	/* 49 - 56 */
	 15, 15, 15, 15, 15, 15, 15, 15};	/* 57 - 64 */

/* map the sanitized data length to an appropriate data length code */
unsigned char can_fd_len2dlc(unsigned char len)
{
	if (len > 64)
		return 0xF;

	return len2dlc[len];
}

static unsigned char hexval(char c)
{
	if ((c >= '0') && (c <= '9'))
		return c - '0';
	if ((c >= 'A') && (c <= 'F'))
		return c - 'A' + 10;
	if ((c >= 'a') && (c <= 'f'))
		return c - 'a' + 10;

	/* error */
	return 16;
}

/*
 * CAN frame string format
 *
 * In a standard frame, identifier is 11bit, represented as hex takes
 * 3 bytes
 * In an extended frame, identifier is
 *
 * CID#TXXXXXXXX
 * CIDEXTEN#TXXXXXXXX
 *
 * CID can id, 3 bytes, each is a hex char
 * T frame type (r = rtr,
 *
 */
static int parse_canframe(char *cs, struct canfd_frame *cf)
{
	int i, idx, dlen, len;
	int maxdlen = CAN_MAX_DLEN;
	int ret = CAN_MTU;
	unsigned char tmp;

	len = strlen(cs);
	if (len < 4)
		return 0;

	/* init CAN FD frame, e.g. LEN = 0 */
	memset(cf, 0, sizeof(*cf));

	if (cs[3] == CANID_DELIM) {
		idx = 4;
		for (i = 0; i < 3; i++){
			if ((tmp = hexval(cs[i])) > 0x0F)
				return 0;
			cf->can_id |= (tmp << (2 - i) * 4);
		}
	} else if (cs[8] == CANID_DELIM) { /* 8 digits */
		idx = 9;
		for (i = 0; i < 8; i++){
			if ((tmp = hexval(cs[i])) > 0x0F)
				return 0;
			cf->can_id |= (tmp << (7 - i) * 4);
		}
		/* 8 digits but no errorframe ? */
		if (!(cf->can_id & CAN_ERR_FLAG))
			/* then it is an extended frame */
			cf->can_id |= CAN_EFF_FLAG;
	} else
		return 0;

	/* RTR frame ? */
	if((cs[idx] == 'R') || (cs[idx] == 'r')){
		cf->can_id |= CAN_RTR_FLAG;

		/* check for optional DLC value for CAN 2.0B frames */
		if(cs[++idx] && (tmp = hexval(cs[idx++])) <= CAN_MAX_DLEN) {
			cf->len = tmp;

			/* check optional raw DLC value for CAN 2.0B frames */
			if ((tmp == CAN_MAX_DLEN)
				&& (cs[idx++] == CC_DLC_DELIM)) {
				tmp = hexval(cs[idx]);
				if ((tmp > CAN_MAX_DLEN) &&
					(tmp <= CAN_MAX_RAW_DLC)) {
					struct can_frame *ccf =
						(struct can_frame *)cf;

					ccf->len8_dlc = tmp;
				}
			}
		}
		return ret;
	}

	/* CAN FD frame escape char '##' ? */
	if (cs[idx] == CANID_DELIM) {

		maxdlen = CANFD_MAX_DLEN;
		ret = CANFD_MTU;

		/* CAN FD frame <canid>##<flags><data>* */
		if ((tmp = hexval(cs[idx + 1])) > 0x0F)
			return 0;

		cf->flags = tmp;
		idx += 2;
	}

	for (i = 0, dlen = 0; i < maxdlen; i++) {

		/* skip (optional) separator */
		if(cs[idx] == DATA_SEPERATOR)
			idx++;

		 /* end of string => end of data */
		if(idx >= len)
			break;

		if ((tmp = hexval(cs[idx++])) > 0x0F)
			return 0;
		cf->data[i] = (tmp << 4);
		if ((tmp = hexval(cs[idx++])) > 0x0F)
			return 0;
		cf->data[i] |= tmp;
		dlen++;
	}
	cf->len = dlen;

	/* check for extra DLC when having a Classic CAN with 8 bytes payload */
	if ((maxdlen == CAN_MAX_DLEN) && (dlen == CAN_MAX_DLEN)
		&& (cs[idx++] == CC_DLC_DELIM)) {
		unsigned char dlc = hexval(cs[idx]);

		if ((dlc > CAN_MAX_DLEN) && (dlc <= CAN_MAX_RAW_DLC)) {
			struct can_frame *ccf = (struct can_frame *)cf;

			ccf->len8_dlc = dlc;
		}
	}

	return ret;
}

static int send_frame(char *iface, char *data)
{
	int s, rval = 0;
	struct ifreq ifr;
	struct sockaddr_can addr;
	struct canfd_frame frame;
	int mtu, required_mtu;
	int enable_canfd = 1;

	printf("%s() iface:%s data:%s\n", __func__, iface, data);

	required_mtu = parse_canframe(data, &frame);
	if (!required_mtu){
		fprintf(stderr, "\nWrong CAN-frame format!\n\n");
		usage();
		return 1;
	}

	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("socket");
		return 1;
	}

	strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);
	ifr.ifr_name[IFNAMSIZ - 1] = '\0';
	ifr.ifr_ifindex = if_nametoindex(ifr.ifr_name);
	if (!ifr.ifr_ifindex) {
		perror("if_nametoindex");
		return 1;
	}

	memset(&addr, 0, sizeof(addr));
        addr.can_family = AF_CAN;
        addr.can_ifindex = ifr.ifr_ifindex;

	/* Can be 0, CAN_MTU, CANFD_MTU */
	if (required_mtu > (int)CAN_MTU) {

		/* check if the frame fits into the CAN netdevice */
		if (ioctl(s, SIOCGIFMTU, &ifr) < 0) {
			perror("SIOCGIFMTU");
			return 1;
		}
		mtu = ifr.ifr_mtu;

		if (mtu != CANFD_MTU) {
			printf("CAN interface is not CAN FD capable, sorry.\n");
			return 1;
		}

		/* interface is ok
		 * try to switch the socket into CAN FD mode */
		if (setsockopt(s, SOL_CAN_RAW, CAN_RAW_FD_FRAMES,
			       &enable_canfd, sizeof(enable_canfd))){
			printf("error when enabling CAN FD support\n");
			return 1;
		}

		/* ensure discrete CAN FD length values
		 * 0..8, 12, 16, 20, 24, 32, 64 */
		frame.len = can_fd_dlc2len(can_fd_len2dlc(frame.len));
	}

	/* save some cpu usage */
	setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}

	/* send frame */
	if (write(s, &frame, required_mtu) != required_mtu) {
		perror("write");
		close(s);
		printf("exiting ...\n");

		return 1;
	}

err:
	rval = 1;

	close(s);

	return rval;
}

int main(int argc, char **argv)
{
	int c;

	if (argc != 2 && argc != 3)
		usage();

        while (1) {
                int this_option_optind = optind ? optind : 1;
                int option_index = 0;
                static struct option long_options[] = {
                        {"help", no_argument, 0, 'h'},
                        {0, 0, 0, 0}
                };

                c = getopt_long(argc, argv, "h",
                                long_options, &option_index);
                if (c == -1)
                        break;

                switch (c) {
                case 'h':
                        usage();
                        break;
                default:
                        _exit(2);
                }
        }

	return send_frame(argv[1], argv[2]);
}
